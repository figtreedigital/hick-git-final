<?php
/*
Template Name: Page - Right Sidebar
*/
?>

<?php get_header(); ?>

        <div id="content" class="clearfix">
        
          <div id="main" class="clearfix" role="main">

         <?php

// check if the flexible content field has rows of data
if( have_rows('content_types') ):

     // loop through the rows of data
    while ( have_rows('content_types') ) : the_row(); ?>

       <?php if( get_row_layout() == 'left_image_right_content' ): ?>
            
            
             	<section class="section-wrap content nopadding" >
        	
        <div class="banner">
        	    	<div class="is-table-row">
                    <?php $image = get_sub_field( 'image' ); ?>
			<?php if ( $image ) { ?>
        	        <div class="col-xs-12 col-sm-12 col-md-6 image-wrap wow slideInLeft" style="background:url(<?php echo $image['sizes']['thumbnail-large']; ?>);" ></div>
        	        <?php } ?>
        	        <div class="col-xs-12 col-sm-12 col-md-6 wow slideInRight"><div class="text-banner-wrap"><?php the_sub_field( 'content' ); ?>
        	                                                        
        	                                                            <?php $link = get_sub_field( 'botton_link' ); ?>
        	                                                                                    <?php if ( $link ) { ?>
        	                                                                                        <a class="arrow-botton" href="<?php the_sub_field('botton_link'); ?>"><?php the_sub_field('button_text'); ?><img width="65" height="65" src="<?php echo get_template_directory_uri(); ?>/library/images/Hicks-Turf-Icon-Arrow-Green.svg" alt="Turf Varieties"></a>
        	                                                                                    <?php } ?></div></div>
        	       
        	    </div>
        	</div>
        	
        	
        		
        		
        	
        	
        	</section>
        	
            

        	
        	
        	 <?php  elseif( get_row_layout() == 'left_content_right_image' ): ?>
        	 
        	 
        	
        	<section class="section-wrap content nopadding" >
        	
        <div class="banner">
        	    	<div class="is-table-row">
                   <?php $image = get_sub_field( 'image' ); ?>
			<?php if ( $image ) { ?>
        	        <div class="col-xs-12 col-sm-12 col-md-6 col-md-push-6 image-wrap wow slideInRight" style="background:url(<?php echo $image['sizes']['thumbnail-large']; ?>);" ></div>
        	           <?php } ?>
        	        <div class="col-xs-12 col-sm-12 col-md-6 col-md-pull-6 wow slideInLeft"><div class="text-banner-wrap"><?php the_sub_field( 'content' ); ?>
        	                                                        
        	                                                            <?php $link = get_sub_field( 'botton_link' ); ?>
        	                                                                                    <?php if ( $link ) { ?>
        	                                                                                        <a class="arrow-botton" href="<?php the_sub_field('botton_link'); ?>"><?php the_sub_field('button_text'); ?><img width="65" height="65" src="<?php echo get_template_directory_uri(); ?>/library/images/Hicks-Turf-Icon-Arrow-Green.svg" alt="Turf Varieties"></a>
        	                                                                                    <?php } ?></div></div>
        	       
        	    </div>
        	</div>
        	
        	
        		
        		
        	
        	
        	</section>
        	
        	<?php  elseif( get_row_layout() == 'image_banner' ): ?>
        	
        	<?php $image = get_sub_field( 'image' ); ?>
        	
        	<section class="section-wrap wow fadeIn">
        	
             <div class="image-banner" style="background-image: url(<?php echo $image['sizes']['banner']; ?>);"></div>
             
             
              </section>
              
              <?php elseif ( get_row_layout() == 'full_width_content' ) : ?>
              <section class="section-wrap content padding30 wow fadeIn <?php the_sub_field( 'padding_top' ); ?>">
			<div class="container"><?php the_sub_field( 'content' ); ?></div>
              </section>
              
                <?php elseif ( get_row_layout() == 'full_width_content-grey' ) : ?>
              <section class="section-wrap content padding30 grey wow fadeIn <?php the_sub_field( 'padding_top' ); ?>">
			<div class="container"><?php the_sub_field( 'content' ); ?></div>
              </section>
              
              <?php elseif ( get_row_layout() == 'map' ) : ?>
              <section class="section-wrap content wow fadeIn map">
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDf3h8vfa_VmbTvQppCCstFR_sOMINKt3I"></script>

     <script type="text/javascript">
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);

            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 15,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(-34.7939098, 138.6472546), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                     styles: [{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"simplified"},{"hue":"#ff0000"}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                     /*
   * add markers to map
   */

    var image = {
        url: '<?php echo get_template_directory_uri(); ?>/library/images/map-icon.png', // image is 512 x 512
        scaledSize : new google.maps.Size(50, 50),
    };

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(-34.7939098, 138.6472546),
                    map: map,
                    title: 'Back Centre',
                     icon: image
                });


            }
        </script>

 <div class="map-wrap notopmargin sm-wrap-margins">

            <div id="map"></div>
      </div>
              </section>
    
        	
        
    
  

    <?php  endif;

    endwhile;

else :

    // no layouts found

endif; ?>
        
          </div> <!-- end #main -->

          
      
        </div> <!-- end #content -->


<?php get_footer(); ?>
