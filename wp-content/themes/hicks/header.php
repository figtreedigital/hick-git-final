<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // Google Chrome Frame for IE ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?php if (is_front_page()) { wp_title(''); } else { wp_title(''); } ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicon-16x16.png">
<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/manifest.json">
<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/font-awesome/css/font-awesome.min.css">
		
		<script src="https://use.typekit.net/rwi4pcs.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>

	</head>

	<body <?php body_class(); ?>>

    <header class="header">

      <nav role="navigation">
        <div class="navbar-fixed-top"><div class="navbar white-nav">
            
            
                        <!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
                               <div class="bottom-align-text col-xs-12 col-sm-6 col-md-6 col-lg-6">
                         <div class="navbar-header">
                          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                        
                          <a class="logo" href="<?php bloginfo( 'url' ) ?>/" title="<?php bloginfo( 'name' ) ?>" rel="homepage"><img src="<?php echo get_template_directory_uri(); ?>/library/images/hicks-instant-turf-logo.svg" alt="Hicks Instant Turf" width="345" height="219"></a>
                        
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <p class="info hidden-xs-down"><strong style="font-weight: 600;">Talk to the Turf Experts <span>08 8258 2488</span></strong><br><i>South Australia’s only 24hr turf display centre</i></p>
                        
                    </div>
              </div>
            
                       
                       <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 menu-wrap">
                           
                            <div class="navbar-collapse collapse navbar-responsive-collapse">
                              <?php bones_main_nav(); ?>
                            
                            </div>
                        </div>
                        
                       
                    
                    
                
             
            </div> </div>
        
      </nav>

		</header> <?php // end header ?>
