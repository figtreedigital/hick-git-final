
     <section class="section-wrap footer-grid padding40">

        <div class="footer-grid-wrap">
          
          <ul class="gallery">
           
           <li class="wow slideInLeft" data-wow-duration="2s" data-wow-delay="0s">
              
                   
                   <div class="thumb">
                       <a title="Turf Calculator" href="<?php bloginfo( 'url' ) ?>/turf-calculator/"><img class="icon" width="190" height="230" src="<?php echo get_template_directory_uri(); ?>/library/images/Hicks-Turf-Icon-Calculator-White.svg" alt="Turf Calculator Adelaide"></a>
                       <h3>Turf Calculator</h3>
                       <h4>Work out how much you need</h4>
                       <a title="Turf Calculator" href="<?php bloginfo( 'url' ) ?>/turf-calculator/">MORE INFO<img width="30" height="25" src="<?php echo get_template_directory_uri(); ?>/library/images/Hicks-Turf-Icon-Arrow-White.svg" alt="Turf calculator"></a>
                   </div>
                   
               
           </li>
           
           <li class="wow slideInLeft" data-wow-duration="2s" data-wow-delay="0s">
           
               
               <div class="thumb">
                   <a title="Gift Vouchers" href="<?php bloginfo( 'url' ) ?>/gift-voucher/"> <img class="icon" width="190" height="230" src="<?php echo get_template_directory_uri(); ?>/library/images/Hicks-Turf-Icon-Gift-White.svg" alt="Lawn and Turf Adelaide"></a>
                   <h3>GIFT VOUCHERS</h3>
                   <h4>Give the gift of a perfect lawn</h4>
                   <a title="Gift Vouchers" href="<?php bloginfo( 'url' ) ?>/gift-voucher/">PURCHASE<img width="30" height="25" src="<?php echo get_template_directory_uri(); ?>/library/images/Hicks-Turf-Icon-Arrow-White.svg" alt="Turf lawn"></a>
               </div>
               
           
              </li>
           
           <li class="wow slideInLeft" data-wow-duration="2s" data-wow-delay="0s">
         
               
               <div class="thumb">
                   <a title="Book turf Delivery" href="<?php bloginfo( 'url' ) ?>/contact"><img class="icon" width="190" height="230" src="<?php echo get_template_directory_uri(); ?>/library/images/Hicks-Turf-Icon-Delivery-White.svg" alt="Turf Delivery Adelaide"></a>
                   <h3>NEXT DAY DELIVERY</h3>
                   <h4>Book before 12 noon Tues–Sat<sup>*</sup></h4>
                   <a title="Book turf Delivery" href="<?php bloginfo( 'url' ) ?>/contact">BOOK NOW<img width="30" height="25" src="<?php echo get_template_directory_uri(); ?>/library/images/Hicks-Turf-Icon-Arrow-White.svg" alt="Turf delivery adelaide"></a>
                   <p><sup>*</sup>T&Cs apply</p>
               </div>
               
           
              </li>
           
           </ul>
            
        </div> 

     </section>

             <footer id="footer" class="clearfix" >
      <div id="footer-widgets">

        <div class="footer-container">

        <div id="footer-wrapper">

          <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-2">
              <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-1') ) : ?>
              <?php endif; ?>
            </div> <!-- end widget1 -->

            <div class="col-sm-6 col-md-6 col-lg-3">
              <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-2') ) : ?>
              <?php endif; ?>
            </div> <!-- end widget1 -->

            <div class="col-sm-6 col-md-6 col-md-offset-0 col-lg-3 col-mdlgoffset-1">
              <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-3') ) : ?>
              <?php endif; ?>
            </div> <!-- end widget1 -->

            <div class="col-sm-6 col-md-6 col-lg-3">
              <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-4') ) : ?>
              <?php endif; ?>
               <img width="120" height="70" src="<?php echo get_template_directory_uri(); ?>/library/images/Hicks-Instant-Turf-Adelaide.svg" alt="Hicks Instant Turf Adelaide">
            </div> <!-- end widget1 -->

          </div> <!-- end .row -->

        </div> <!-- end #footer-wrapper -->

        </div> <!-- end .container -->
      </div> <!-- end #footer-widgets -->

      <div id="sub-floor">
        <div class="container">
          <div class="row">
            <div class="col-md-12 copyright">
              &copy; Copyright <?php echo date('Y'); ?>, Hicks Instant Turf. All Rights Reserved. Website by <a title="Fig Tree Digital Website Design Adelaide" target="_blank" href="https://figtreedigital.com.au/">Fig Tree Digital</a>
            </div>
        
          </div> <!-- end .row -->
        </div>
      </div>

    </footer> <!-- end footer -->

    <!-- all js scripts are loaded in library/bones.php -->
    <?php wp_footer(); ?>
    <!-- Hello? Doctor? Name? Continue? Yesterday? Tomorrow?  -->

  </body>

</html> <!-- end page. what a ride! -->
