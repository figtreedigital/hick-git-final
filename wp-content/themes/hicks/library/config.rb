require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "wp-content/themes/sarahshanahan/library/css"
sass_dir = "wp-content/themes/sarahshanahan/library/sass"
images_dir = "wp-content/themes/sarahshanahan/library/images"
javascripts_dir = "wp-content/themes/sarahshanahan/library/js"
fonts_dir = "wp-content/themes/sarahshanahan/library/fonts"

output_style = :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

line_comments = false
color_output = false

preferred_syntax = :scss
