<?php
/*
Template Name: Home Page Template
*/
?>

<?php get_header(); ?>

<div class="slider-pro wow fadeIn" id="home-slider" data-wow-duration="2s" data-wow-delay="0s">
	<div class="sp-slides">
	
	
	<?php if ( have_rows( 'slider' ) ) : ?>
	<?php while ( have_rows( 'slider' ) ) : the_row(); ?>
		<?php $image = get_sub_field( 'image' ); ?>
		<?php if ( $image ) { ?>
		<!-- Slide 1 -->
		<div class="sp-slide"  style="background:url(<?php echo $image['sizes']['banner']; ?>);" >
		
		<div class="slide-container wow fadeIn" data-wow-duration="2s" data-wow-delay="1s">
		    <h2><?php the_sub_field( 'heading' ); ?></h2>
		<h3><?php the_sub_field( 'sub_heading' ); ?></h3>
		<?php $link = get_sub_field( 'link' ); ?>
		<?php if ( $link ) { ?>
			<a class="findoutmore" href="<?php echo $link['url']; ?>" ><?php echo $link['title']; ?>FIND OUT MORE <img width="30" height="25" src="<?php echo get_template_directory_uri(); ?>/library/images/Hicks-Turf-Icon-Arrow-White.svg" alt="Turf delivery adelaide"></a>
		<?php } ?>
		</div>
			
		</div>
		
		<?php } ?>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>
		

	</div>
</div>


<section class="home-content wow fadeIn"  data-wow-duration="2s" data-wow-delay="0s">
      <div class="home-containerp padding60bottom padding20top">
        
           <?php if ( have_rows( 'home_content' ) ) : ?>
          <?php while ( have_rows( 'home_content' ) ) : the_row(); ?>
              
           <img src="<?php echo get_template_directory_uri(); ?>/library/images/Hicks-Turf-Icon-Australia-Green.svg" width="175" height="175" alt="Australia’s Instant Turf - Adelaide">
            
            <h1> <?php the_sub_field( 'main_heading' ); ?></h1>
          
              <div class="about-home-content homecontent"><?php the_sub_field( 'about_content' ); ?></div>
              
              <a class="btn btn-default" href="<?php bloginfo( 'url' ) ?>/contact">CONTACT HICKS INSTANT TURF</a>
              
          <?php endwhile; ?>
      <?php endif; ?>
          
        
      </div>
  </section>
  
  
   
   
   <?php

// check if the flexible content field has rows of data
if( have_rows('content_types') ):

     // loop through the rows of data
    while ( have_rows('content_types') ) : the_row(); ?>

       <?php if( get_row_layout() == 'left_image_right_content' ): ?>

        	 	<section class="section-wrap content nopadding" >
        	 	
        	 	<?php $image = get_sub_field( 'image' ); ?>
			<?php if ( $image ) { ?>
        	
        <div class="banner">
        	    	<div class="is-table-row">
        	        <div class="col-xs-12 col-sm-12 col-md-6 image-wrap wow slideInLeft" style="background:url(<?php echo $image['sizes']['thumbnail-large']; ?>);" ></div>
        	        
        	        <?php } ?>
        	        
        	        <div class="col-xs-12 col-sm-12 col-md-6 wow slideInRight"><div class="text-banner-wrap"><?php the_sub_field( 'content' ); ?>
        	                                                        
        	                                                            <?php $link = get_sub_field( 'botton_link' ); ?>
        	                                                                                    <?php if ( $link ) { ?>
        	                                                                                        <a class="arrow-botton" href="<?php the_sub_field('botton_link'); ?>"><?php the_sub_field('button_text'); ?><img width="65" height="65" src="<?php echo get_template_directory_uri(); ?>/library/images/Hicks-Turf-Icon-Arrow-Green.svg" alt="Turf Varieties"></a>
        	                                                                                    <?php } ?></div></div>
        	       
        	    </div>
        	</div>
        	
        	
        		
        		
        	
        	
        	</section>
        	
        	 <?php  elseif( get_row_layout() == 'left_content_right_image' ): ?>
        	
           	<section class="section-wrap content nopadding" >
        	
        <div class="banner">
        	    	<div class="is-table-row">
                   <?php $image = get_sub_field( 'image' ); ?>
			<?php if ( $image ) { ?>
        	        <div class="col-xs-12 col-sm-12 col-md-6 col-md-push-6 image-wrap wow slideInRight" style="background:url(<?php echo $image['sizes']['thumbnail-large']; ?>);" ></div>
        	        <?php } ?>
        	        <div class="col-xs-12 col-sm-12 col-md-6 col-md-pull-6 wow slideInLeft"><div class="text-banner-wrap"><?php the_sub_field( 'content' ); ?>
        	                                                        
        	                                                            <?php $link = get_sub_field( 'botton_link' ); ?>
        	                                                                                    <?php if ( $link ) { ?>
        	                                                                                        <a class="arrow-botton" href="<?php the_sub_field('botton_link'); ?>"><?php the_sub_field('button_text'); ?><img width="65" height="65" src="<?php echo get_template_directory_uri(); ?>/library/images/Hicks-Turf-Icon-Arrow-Green.svg" alt="Turf Varieties"></a>
        	                                                                                    <?php } ?></div></div>
        	       
        	    </div>
        	</div>
        	
        	
        		
        		
        	
        	
        	</section>

    
        	    <?php elseif ( get_row_layout() == 'full_width_content-grey' ) : ?>
              <section class="section-wrap content padding20 grey wow fadeIn">
			<div class="container"><?php the_sub_field( 'content' ); ?></div>
              </section>
        
    
  

    <?php  endif;

    endwhile;

else :

    // no layouts found

endif; ?>

    






<?php get_footer(); ?>
